require 'test_helper'

# session_helperのテストだから、session_heplerメソッドが使えるのかな？？
class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:michael)
    remember(@user)
    # => digest登録 & cookie発行
  end
  
  # sessionの方は、テストされてるっぽかったので、cookie ver~のみテスト
  
  # cookie認証版current_userテスト
  test "current_user returns right user when session is nil" do
    # assert_equal 期待する値, 実際の値
    assert_equal @user, current_user
    assert is_logged_in?
  end

  # authenticated?メソッドのテストを踏まえつつ、current_userのテスト失敗版
  test "current_user returns nil when remember digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
end