ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper

  def is_logged_in?
    !session[:user_id].nil?
    # not 『nilですか？』 == true => nilじゃない！！！
    # => loginしてるってこと！
  end
  
  # テストユーザーとしてログインする（単体テスト => login = sessionの発行）
  def log_in_as(user)
    session[:user_id] = user.id
    # => sessionの発行 = login
  end

end  
  
class ActionDispatch::IntegrationTest
  
  # テストユーザーとしてログインする（統合テスト => viewの動作確認）
  def log_in_as(user, password: "password", remember_me: "1")
    # Sessions#create
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me}}
  end
end
