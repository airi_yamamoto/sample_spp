require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # signupのview用のテスト
  def setup
    ActionMailer::Base.deliveries.clear
  end
  
  test "invalid signup info" do
    get signup_path
    assert_select "form[action = ?]",signup_path
    
    # ブロック内の処理(不正なparams)を実施しても、User.countが変化しない
    assert_no_difference "User.count" do
      post signup_path, params: {user: { name:  "",
                                     email: "user@invalid",
                                     password:              "foo",
                                     password_confirmation: "bar" } }
    end
    assert_template "users/new"
    
    # rails にあるやり方をコピペしたらうまくいかなかった！
        # 『#とか.後に、<>入れちゃだめです』って！
        # slimっぽく書いたら成功！イェイ！
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end


  test "valid signup infomation with account activation" do
    get signup_path
    assert_select "form[action = ?]",signup_path
    assert_difference "User.count",1 do
      post signup_path, params: {user: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user) # => users#create @user
    assert_not user.activated?
    # 有効化してない状態でのlogin => falseになるはず
    log_in_as(user)
    assert_not is_logged_in?
    # 有効化トークンが不正な場合 => AccountActivation#edit に不正なurlが送信された
    get edit_account_activation_path("invalid token", email: user.email)
    assert_not is_logged_in?
    # トークンは正しいがメールアドレスが無効な場合
    get edit_account_activation_path(user.activation_token, email: "wrong")
    assert_not is_logged_in?
    # 有効化トークンが正しい場合
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end

end