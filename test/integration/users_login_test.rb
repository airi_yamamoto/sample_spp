require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "login with invalid infomation" do
    get login_path
    assert_template "sessions/new"
    post login_path,params:{session: {email: "", password: ""}}
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  
  test "login with valid information followed by logout" do
    get login_path
    post login_path,params: {session: { email: @user.email,
                                        password: 'password' } }
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href = ? ]", login_path, count: 0
    assert_select "a[href = ? ]", logout_path
    assert_select "a[href = ? ]", user_path(@user)
    
    # 以下log_out
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_path
    # 2番目のウィンドウでログアウトをクリックするユーザーをシミュレートする
    delete logout_path
    follow_redirect!  
    assert_select "a[href = ? ]", login_path
    assert_select "a[href = ? ]", logout_path ,count: 0
    assert_select "a[href = ? ]", user_path(@user),count: 0
  end
  
  # cookies機能のテスト
  test "login with remembering" do
    # Sessions#create へのログインリクエスト
    log_in_as(@user,remember_me: "1")
    # Sessions#create で@userが使われていれば、テストでassigns(:user)としてuser情報使用可能
    assert_equal cookies[:remember_token], assigns(:user).remember_token
  end
  
  test "login without remembering" do
    # クッキーを焼く
    log_in_as(@user,remember_me: "1")
    delete logout_path
    # 今度はクッキーは焼かないぞ！
    log_in_as(@user,remember_me: "0")
    assert_empty cookies["remember_token"]
  end
  
end
