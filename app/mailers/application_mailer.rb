class ApplicationMailer < ActionMailer::Base
  # from: "mail送信元のアドレス"
  default from: 'noreply@example.com'
  layout 'mailer'
end
